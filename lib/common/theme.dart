import 'package:flutter/material.dart';

var theme = ThemeData(
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w500,
      color: Color.fromARGB(255, 58, 58, 58)
    ),
    titleMedium: TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.w500,
        color: Color.fromARGB(255, 129, 129, 129)
    ),
    labelMedium: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w700,
        color: Colors.white
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      backgroundColor: const Color.fromARGB(255, 106, 139, 249),
      disabledBackgroundColor: const Color.fromARGB(255, 167, 167, 167),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      ),
    )
  ),
  checkboxTheme: CheckboxThemeData(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(8)
    ),
    side: const BorderSide(
      width: 1,
      color: Color.fromARGB(255, 129, 129, 129)
    ),
  )
);