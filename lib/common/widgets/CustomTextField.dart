import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final bool enableObscure;
  final TextEditingController controller;
  final Function(String) onChanged;
  const CustomTextField({super.key, required this.label, required this.hint, this.enableObscure = false, required this.controller, required this.onChanged});



  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}
bool isObscure = true;
class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 24),
        Row(
          children: [
            Text(
              widget.label,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ],
        ),
        const SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: "*",
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color.fromARGB(255, 129, 129, 129), width: 1),
                borderRadius: BorderRadius.circular(4),
              ),
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(color: Color.fromARGB(
                  255, 207, 207, 207)),
              contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 10),
              suffixIcon: (widget.enableObscure) ? GestureDetector(
                onTap: (){setState(() {
                  isObscure = !isObscure;
                });},
                child: Image.asset('assets/eye-slash.png'),
              ) : null
            ),
            onChanged: widget.onChanged,
          ),
        )
      ],
    );
  }
}