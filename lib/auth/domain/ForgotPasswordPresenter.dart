import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';
import 'package:try10/auth/domain/utils.dart';


bool isValidForgot(String email){
  return email.isNotEmpty && correctEmail(email) ;
}

void pressSendOTP(String email, Function onResponse, Function onError){
  try{
    sendOTP(email);
    onResponse();
  }on AuthException catch(e){
    onError();
  }
}