import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';

bool isValidNew(String password, String confirmPassword){
  return password.isNotEmpty && confirmPassword.isNotEmpty && password == confirmPassword;
}

void pressChange(String password, Function onResponse, Function onError){
  try{
    changePassword(password);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}