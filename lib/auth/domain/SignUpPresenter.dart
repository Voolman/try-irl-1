import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';
import 'package:try10/auth/domain/utils.dart';

bool isValidSignUp(String email, String password, String confirmPassword){
  return email.isNotEmpty && password.isNotEmpty && confirmPassword.isNotEmpty &&  password == confirmPassword;
}

void pressSignUp(String email, String password, Function onResponse, Function onError){
  try{
    signUp(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}