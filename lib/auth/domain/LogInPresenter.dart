import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';
import 'package:try10/auth/domain/utils.dart';

bool isValidLogIn(String email, String password){
  return email.isNotEmpty && password.isNotEmpty && correctEmail(email);
}

void pressSignIn(String email, String password, Function onResponse, Function onError){
  try{
    signIn(email, password);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}