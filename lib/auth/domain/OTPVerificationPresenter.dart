import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';

bool isValidOTPVerif(String code){
  return code.length == 6;
}

void pressVerifyOTP(String email, String code, Function onResponse, Function onError){
  try{
    verifyOTP(email, code);
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}