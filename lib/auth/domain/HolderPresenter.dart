import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:try10/auth/data/repository/supabase.dart';

void pressSignOut(Function onResponse, Function onError){
  try{
    signOut();
    onResponse();
  }on AuthException catch(e){
    onError(e);
  }
}