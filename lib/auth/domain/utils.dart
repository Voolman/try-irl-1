import 'package:flutter/material.dart';

bool correctEmail(String email){
  return RegExp(r'^[0-9a-z]+@[0-9a-z]+\.\w{2,}$').hasMatch(email);
}

void showError(BuildContext context,String e){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text("Ошибка"),
    content: Text(e),
    actions: [
      TextButton(
          onPressed: (){Navigator.of(context).pop;},
          child: const Text('OK'))
    ],
    )
  );
}