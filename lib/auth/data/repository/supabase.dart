import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<void> signUp(String email, String password) async {
  await supabase.auth.signUp(email: email, password: password);
}

Future<void> signIn(String email, String password) async {
  await supabase.auth.signInWithPassword(email: email, password: password);
}

Future<void> signOut() async {
  await supabase.auth.signOut();
}

Future<void> sendOTP(String email) async {
  await supabase.auth.resetPasswordForEmail(email);
}

Future<void> verifyOTP(String email,String code) async {
  await supabase.auth.verifyOTP(email: email, token: code, type: OtpType.email);
}


Future<void> changePassword(String password) async {
  await supabase.auth.updateUser(UserAttributes(
    password: password
  ));
}