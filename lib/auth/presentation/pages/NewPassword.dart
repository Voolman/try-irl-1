import 'package:flutter/material.dart';
import 'package:try10/auth/domain/NewPasswordPresenter.dart';
import 'package:try10/auth/presentation/pages/Holder.dart';
import 'package:try10/auth/presentation/pages/LogIn.dart';
import 'package:try10/common/widgets/CustomTextField.dart';

import '../../domain/utils.dart';

class NewPassword extends StatefulWidget {
  const NewPassword({super.key});

  @override
  State<NewPassword> createState() => _NewPasswordState();
}
TextEditingController password = TextEditingController();
TextEditingController confirmPassword = TextEditingController();

class _NewPasswordState extends State<NewPassword> {

  bool isValid = false;
  void onChanged(_){
    setState(() {
      isValid = isValidNew(password.text, confirmPassword.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Новый пароль',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Введите новый пароль',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4,),
                CustomTextField(
                    label: "Пароль",
                    hint: '**********',
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Повторите пароль",
                    hint: '**********',
                    enableObscure: true,
                    controller: confirmPassword,
                    onChanged: onChanged
                ),
                const SizedBox(height: 411),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid) ? (){
                      pressChange(
                          password.text,
                              (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                              (String e){showError(context, e);}
                      );
                    } : null,
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Подтвердить',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'Я вспомнил свой пароль! ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                        ),
                        TextSpan(
                            text: 'Вернуться',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: const Color.fromARGB(255, 106, 139, 249))
                        )
                      ]
                  )
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}