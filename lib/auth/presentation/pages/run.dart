import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'SignUp.dart';
import '../../../common/theme.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://rccpniqgvjlzmiijaxaf.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJjY3BuaXFndmpsem1paWpheGFmIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTA1MDk3OTMsImV4cCI6MjAyNjA4NTc5M30.h0sd-BSKG7KSiOybuCM1gAfQyLjAAIxfE8heaXMgLfU',
  );

  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: theme,
      home: const SignUp(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});



  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}
