import 'dart:io';

import 'package:flutter/material.dart';
import 'package:try10/auth/domain/HolderPresenter.dart';

import '../../domain/utils.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}
TextEditingController email = TextEditingController();
TextEditingController password = TextEditingController();
TextEditingController confirmPassword = TextEditingController();

class _HolderState extends State<Holder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SizedBox(
          height: 46,
          width: 342,
          child: FilledButton(
            onPressed: () {
              pressSignOut(
                      (){exit(0);},
                      (String e){showError(context, e);}
              );
            },
            style: Theme.of(context).filledButtonTheme.style,
            child: Text(
              'ВЫХОД',
              style: Theme.of(context).textTheme.labelMedium,
            ),
          ),
        ),
      )
    );
  }
}