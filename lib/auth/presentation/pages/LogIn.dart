import 'package:flutter/material.dart';
import 'package:try10/auth/domain/LogInPresenter.dart';
import 'package:try10/auth/presentation/pages/ForgotPassword.dart';
import 'package:try10/common/widgets/CustomTextField.dart';

import '../../domain/utils.dart';
import 'Holder.dart';
import 'SignUp.dart';

class LogIn extends StatefulWidget {
  const LogIn({super.key});

  @override
  State<LogIn> createState() => _LogInState();
}
TextEditingController email = TextEditingController();
TextEditingController password = TextEditingController();
bool isChecked = false;

class _LogInState extends State<LogIn> {

  bool isValid = false;
  void onChanged(_){
    setState(() {
      isValid = isValidLogIn(email.text, password.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Добро пожаловать',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Заполните почту и пароль чтобы продолжить',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4,),
                CustomTextField(
                    label: "Почта",
                    hint: '***********@mail.com',
                    controller: email,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Пароль",
                    hint: '**********',
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                const SizedBox(height: 18),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          height: 22,
                          width: 22,
                          child: Checkbox(
                            shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)
                            ),
                            side: const BorderSide(
                                width: 1,
                                color: Color.fromARGB(255, 129, 129, 129)
                            ),
                            value: isChecked,
                            onChanged: (value) {
                              setState(() {
                                isChecked = value!;
                              });
                            },
                            activeColor: const Color.fromARGB(255, 106, 139, 249),
                            checkColor: Colors.white,
                          ),
                        ),
                        const SizedBox(width: 8,),
                        Text(
                          'Запомнить меня',
                          style: Theme.of(context).textTheme.titleMedium,
                        )
                      ],
                    ),
                    GestureDetector(
                      onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const ForgotPassword()));},
                      child: Text(
                        'Забыли пароль?',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249)),
                      ),
                    )                  ],
                ),
                const SizedBox(height: 371,),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid) ? (){pressSignIn(
                        email.text,
                        password.text,
                            (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                            (String e){showError(context, e);}
                    );} : null,
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Войти',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUp()));},
                  child: RichText(text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'У меня нет аккаунта! ',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                      ),
                      TextSpan(
                        text: 'Создать',
                          style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: const Color.fromARGB(255, 106, 139, 249))
                      )
                    ]
                    )
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}