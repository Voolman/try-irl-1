import 'package:flutter/material.dart';
import 'package:try10/auth/domain/HolderPresenter.dart';
import 'package:try10/auth/domain/utils.dart';
import 'package:try10/auth/presentation/pages/Holder.dart';
import 'package:try10/auth/presentation/pages/LogIn.dart';
import 'package:try10/common/widgets/CustomTextField.dart';

import '../../domain/SignUpPresenter.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}
TextEditingController email = TextEditingController();
TextEditingController password = TextEditingController();
TextEditingController confirmPassword = TextEditingController();


class _SignUpState extends State<SignUp> {
  bool isValid = false;
  void onChanged(_){
    setState(() {
      isValid = isValidSignUp(email.text, password.text, confirmPassword.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Создать аккаунт',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Завершите регистрацию чтобы начать',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 4,),
                CustomTextField(
                    label: "Почта",
                    hint: '***********@mail.com',
                    controller: email,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Пароль",
                    hint: '**********',
                    enableObscure: true,
                    controller: password,
                    onChanged: onChanged
                ),
                CustomTextField(
                    label: "Повторите пароль",
                    hint: '**********',
                    enableObscure: true,
                    controller: confirmPassword,
                    onChanged: onChanged
                ),
                const SizedBox(height: 319,),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid) ? (){
                      pressSignUp(
                          email.text,
                          password.text,
                          (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const Holder()));},
                          (String e){showError(context, e);}
                      );
                    } : null,
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Зарегистрироваться',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'У меня уже есть аккаунт! ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                        ),
                        TextSpan(
                            text: 'Войти',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: const Color.fromARGB(255, 106, 139, 249))
                        )
                      ]
                  )
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}