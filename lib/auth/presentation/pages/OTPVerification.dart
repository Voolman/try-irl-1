import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:try10/auth/domain/OTPVerificationPresenter.dart';
import 'package:try10/auth/presentation/pages/LogIn.dart';
import 'package:try10/auth/presentation/pages/NewPassword.dart';
import 'package:try10/common/widgets/CustomTextField.dart';

import '../../domain/ForgotPasswordPresenter.dart';
import '../../domain/utils.dart';

class OTPVerification extends StatefulWidget {
  const OTPVerification({super.key});

  @override
  State<OTPVerification> createState() => _OTPVerificationState();
}
TextEditingController code = TextEditingController();

class _OTPVerificationState extends State<OTPVerification> {

  bool isValid = false;
  void onChanged(_){
    setState(() {
      isValid = isValidOTPVerif(code.text);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 83, left: 24, right: 24),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      'Верификация',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      'Введите 6-ти значный код из письма ',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
                const SizedBox(height: 58),
                Pinput(
                  onChanged: onChanged,
                  controller: code,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  length: 6,
                  defaultPinTheme: const PinTheme(
                    height: 32,
                    width: 32,
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 129, 129, 129),
                      borderRadius: BorderRadius.zero
                    )
                  ),
                  submittedPinTheme: const PinTheme(
                      height: 32,
                      width: 32,
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 106, 139, 249),
                          borderRadius: BorderRadius.zero
                      )
                  ),
                ),
                const SizedBox(height: 48),
                GestureDetector(
                  onTap: (){
                    pressSendOTP(
                        email.text,
                            (){},
                            (String e){showError(context, e);}
                    );
                  },
                  child: Text(
                    'Получить новый код',
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(color: const Color.fromARGB(255, 106, 139, 249)),
                  ),
                ),
                const SizedBox(height: 449),
                SizedBox(
                  height: 46,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (isValid) ? (){
                      pressVerifyOTP(
                          email.text,
                          code.text,
                              (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const NewPassword()));},
                              (String e){showError(context, e);}
                      );
                    } : null,
                    style: Theme.of(context).filledButtonTheme.style,
                    child: Text(
                      'Сбросить пароль',
                      style: Theme.of(context).textTheme.labelMedium,
                    ),
                  ),
                ),
                const SizedBox(height: 14),
                GestureDetector(
                  onTap: (){Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => const LogIn()));},
                  child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                            text: 'Я вспомнил свой пароль! ',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w700)
                        ),
                        TextSpan(
                            text: 'Вернуться',
                            style: Theme.of(context).textTheme.titleMedium?.copyWith(fontWeight: FontWeight.w400, color: const Color.fromARGB(255, 106, 139, 249))
                        )
                      ]
                  )
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}